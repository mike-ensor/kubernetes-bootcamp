project = "kubernetes-bootcamp-project"

region = "us-west1"

folder_name = "kubernetes-advanced-bootcamp"

owner_email = "mike.ensor@dev9.com"

credentials = "/../../.config/tf-service-account-key.json"

additional_apis = [
  "compute.googleapis.com",
  "container.googleapis.com",
  "storage-component.googleapis.com",
  "containerregistry.googleapis.com",
  "sqladmin.googleapis.com",
  "sql-component.googleapis.com",
]
