# Overview

* Topics covered in the advanced bootcamp are in addition to and extend the Primary bootcamp

## Agenda

### Advanced Bootcamp

|   Time  | Duration | Topic                                                      |
|:--------|:--------:|:-----------------------------------------------------------|
| 00:00pm |   1h     | Advanced Design Practices (ambassador, init controllers)   |
| 00:00pm |   30m    | Volumes, PersistentVolumeClaims                            |
| 00:00pm |   1h     | Stateful Applications                                      |
| 00:00pm |   30m    | Pod Lifecycle Management                                   |
| 00:00pm |   30m    | Advanced Job Queues & Workloads                            |
| 00:00pm |   1h     | Advanced Scheduling (taints, affinity, tolerance, etc)     |
| 00:00pm |   1h     | Preemptive, Special Nodes (TPU/GPU), Multi-region strategy |
| 00:00pm |   1h     | Non-standard authentication, RBAC & Security               |
