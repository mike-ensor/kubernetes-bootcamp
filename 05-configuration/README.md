# <NAEME>

## Goals

* Understand how to provide configuration values to Pods and Deployment templates
* Utilize Environment Variables
* Replace configuration file for Development and Production instances

## Done
* View configuration
* Expose deployment (`port-forward`) using Pod (container) showing environment variables from ConfigMap
* Deployment + Service of `simple-echo-service-with-config.yaml` has been deployed and is exposed & visible #TODO Link demo container

## Navigation
[Services](../04-services) | [Ingress](../06-ingress "06-ingress lab") | [I need help!!](HELPER.md)

---

### NOTE

* Need a Deployment for Development and Production, but using the same containers
  * Hint: Use Labels & matchSelector & matchExpression
* Need a Service for both

## Exercises

1. Create a Deployment using `eu.gcr.io/kubernetes-bootcamp-belgrade/demo-container:jK0nvKoOSr` image and a Service exposing (NodePort or ClusterIP)
    * View the home page "/" in a browser
    * View "/envvars" endpoint
    * Observe Environment Variables
1. Create & `apply` a `ConfigMap` named _environment-variable-map_ and two keys (see below).  Give them any value
    * env-var-1
    * env.var.1
    * NOTE: the prefix "DEMO_" is requried for front-end to show the environment variable
1. Restart Pods in Deployment (hint: `set image`, `delete -l app=simple-echo-service-app` or `apply -f` with the Deployment file change)
    * Observe the new environment variables
1. Update the `ConfigMap`
    * Did the environment variables webpage change?
    * Reset the Deployment for changes in `ConfigMap` to take effect
1. Create a `ConfigMap` from a properties file
    * Get a list of `ConfigMaps` and Describe the new `ConfigMap`
