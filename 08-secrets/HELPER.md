# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

## Notes

* RBAC enabled clusters require explicit roles to manage `secrets`

## Commands

### Create Secrets

```bash
kubectl apply -f secrets-file.yaml
```

### List Secrets

```bash
kubectl get secret
```

### View a secret

```bash
kubectl describe secret <secret name>
```

```bash
kubectl get secret <secret name> -oyaml
# Take hash from there (or see below)
echo "<hash-from-file>" | base64 --decode
```

### With JSONPath
```bash
# Only works for single item
kubectl get secret ingress-tls-secret -o jsonpath='{.data.*}' | base64 --decode
```
